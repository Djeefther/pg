#pragma once

#include <iostream>

#include <GL/glut.h>

class Vetor {
private:
	GLdouble vec[4];

public:
	Vetor() {
		vec[0] = 0;
		vec[1] = 0;
		vec[2] = 0;
		vec[3] = 0;
	}
	Vetor(GLdouble v1, GLdouble v2, GLdouble v3, GLdouble v4) {
		vec[0] = v1;
		vec[1] = v2;
		vec[2] = v3;
		vec[3] = v4;
	}

	double& operator()(GLint i) {
		return vec[i];
	}
	double operator()(GLint i) const {
		return vec[i];
	}

	Vetor operator+(const Vetor& other) {
		return Vetor(vec[0] + other.vec[0],
			vec[1] + other.vec[1],
			vec[2] + other.vec[2],
			vec[3] + other.vec[3]);
	}

	Vetor operator*(const GLdouble& other) {
		return Vetor(vec[0] + other,
			vec[1] + other,
			vec[2] + other,
			vec[3] + other);
	}
};