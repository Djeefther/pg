#pragma once

#include <iostream>

#include <GL/glut.h>

class Matriz {
private:
	GLdouble mat[16];

public:
	Matriz(double mat16[16]) {
		mat[0] = mat16[0];
		mat[1] = mat16[1];
		mat[2] = mat16[2];
		mat[3] = mat16[3];

		mat[4] = mat16[4];
		mat[5] = mat16[5];
		mat[6] = mat16[6];
		mat[7] = mat16[7];

		mat[8] = mat16[8];
		mat[9] = mat16[9];
		mat[10] = mat16[10];
		mat[11] = mat16[11];

		mat[12] = mat16[12];
		mat[13] = mat16[13];
		mat[14] = mat16[14];
		mat[15] = mat16[15];
	}
	Matriz() {
		mat[0] = 1;
		mat[1] = 0;
		mat[2] = 0;
		mat[3] = 0;

		mat[4] = 0;
		mat[5] = 1;
		mat[6] = 0;
		mat[7] = 0;

		mat[8] = 0;
		mat[9] = 0;
		mat[10] = 1;
		mat[11] = 0;

		mat[12] = 0;
		mat[13] = 0;
		mat[14] = 0;
		mat[15] = 1;
	}

	double& operator()(GLint j, GLint i) {
		return mat[i * 4 + j];
	}
	double operator()(GLint j, GLint i) const {
		return mat[i * 4 + j];
	}

	Matriz operator*(const Matriz& B) {
		Matriz m;

		Matriz& A = *this;

		m(0, 0) = A(0, 0)*B(0, 0) + A(1, 0)*B(0, 1) + A(2, 0)*B(0, 2) + A(3, 0)*B(0, 3);
		m(1, 0) = A(0, 0)*B(1, 0) + A(1, 0)*B(1, 1) + A(2, 0)*B(1, 2) + A(3, 0)*B(1, 3);
		m(2, 0) = A(0, 0)*B(2, 0) + A(1, 0)*B(2, 1) + A(2, 0)*B(2, 2) + A(3, 0)*B(2, 3);
		m(3, 0) = A(0, 0)*B(3, 0) + A(1, 0)*B(3, 1) + A(2, 0)*B(3, 2) + A(3, 0)*B(3, 3);

		m(0, 1) = A(0, 1)*B(0, 0) + A(1, 1)*B(0, 1) + A(2, 1)*B(0, 2) + A(3, 1)*B(0, 3);
		m(1, 1) = A(0, 1)*B(1, 0) + A(1, 1)*B(1, 1) + A(2, 1)*B(1, 2) + A(3, 1)*B(1, 3);
		m(2, 1) = A(0, 1)*B(2, 0) + A(1, 1)*B(2, 1) + A(2, 1)*B(2, 2) + A(3, 1)*B(2, 3);
		m(3, 1) = A(0, 1)*B(3, 0) + A(1, 1)*B(3, 1) + A(2, 1)*B(3, 2) + A(3, 1)*B(3, 3);

		m(0, 2) = A(0, 2)*B(0, 0) + A(1, 2)*B(0, 1) + A(2, 2)*B(0, 2) + A(3, 2)*B(0, 3);
		m(1, 2) = A(0, 2)*B(1, 0) + A(1, 2)*B(1, 1) + A(2, 2)*B(1, 2) + A(3, 2)*B(1, 3);
		m(2, 2) = A(0, 2)*B(2, 0) + A(1, 2)*B(2, 1) + A(2, 2)*B(2, 2) + A(3, 2)*B(2, 3);
		m(3, 2) = A(0, 2)*B(3, 0) + A(1, 2)*B(3, 1) + A(2, 2)*B(3, 2) + A(3, 2)*B(3, 3);

		m(0, 3) = A(0, 3)*B(0, 0) + A(1, 3)*B(0, 1) + A(2, 3)*B(0, 2) + A(3, 3)*B(0, 3);
		m(1, 3) = A(0, 3)*B(1, 0) + A(1, 3)*B(1, 1) + A(2, 3)*B(1, 2) + A(3, 3)*B(1, 3);
		m(2, 3) = A(0, 3)*B(2, 0) + A(1, 3)*B(2, 1) + A(2, 3)*B(2, 2) + A(3, 3)*B(2, 3);
		m(3, 3) = A(0, 3)*B(3, 0) + A(1, 3)*B(3, 1) + A(2, 3)*B(3, 2) + A(3, 3)*B(3, 3);

		return m;
	}

	void translate(GLdouble x, GLdouble y, GLdouble z) {
		Matriz& A = *this;
		Matriz B;

		B(0, 3) = x;
		B(1, 3) = y;
		B(2, 3) = z;

		A = A*B;
	}

	void rotateZ(GLdouble theta) {
		Matriz& A = *this;

		Matriz B;

		B(0, 0) = cos(theta);
		B(0, 1) = -sin(theta);
		B(1, 0) = sin(theta);
		B(1, 1) = cos(theta);

		A = A*B;
	}

	void rotateX(GLdouble theta) {
		Matriz& A = *this;

		Matriz B;

		B(1, 1) = cos(theta);
		B(1, 2) = -sin(theta);
		B(2, 1) = sin(theta);
		B(2, 2) = cos(theta);

		A = A*B;
	}

	void rotateY(GLdouble theta) {
		Matriz& A = *this;

		Matriz B;

		B(0, 0) = cos(theta);
		B(0, 2) = sin(theta);
		B(2, 0) = -sin(theta);
		B(2, 2) = cos(theta);

		A = A*B;
	}

	void rotateW(GLdouble theta, GLdouble wx, GLdouble wy, GLdouble wz) {
		//Normaliza, s� por garantia
		GLdouble sq = sqrt(wx*wx + wy*wy + wz*wz);
		wx = wx / sq;
		wy = wy / sq;
		wz = wz / sq;

		Matriz& A = *this;

		Matriz B;

		B(0, 0) = cos(theta) + wx*wx*(1 - cos(theta));
		B(0, 1) = wx*wy*(1 - cos(theta)) - wz*sin(theta);
		B(0, 2) = wy*sin(theta) + wx*wz*(1 - cos(theta));
		B(1, 0) = wz*sin(theta) + wx*wy*(1 - cos(theta));
		B(1, 1) = cos(theta) + wy*wy*(1 - cos(theta));
		B(1, 2) = -wx*sin(theta) + wy*wz*(1 - cos(theta));
		B(2, 0) = -wy*sin(theta) + wx*wz*(1 - cos(theta));
		B(2, 1) = wx*sin(theta) + wy*wz*(1 - cos(theta));
		B(2, 2) = cos(theta) + wz*wz*(1 - cos(theta));

		A = A*B;
	}

	void scale(GLdouble sx, GLdouble sy, GLdouble sz) {
		Matriz& A = *this;

		Matriz B;

		B(0, 0) = sx;
		B(1, 1) = sy;
		B(2, 2) = sz;

		A = A*B;
	}

	const GLdouble* get() {
		return mat;
	}

	void print() {
		std::cout << mat[0] << " ";
		std::cout << mat[1] << " ";
		std::cout << mat[2] << " ";
		std::cout << mat[3];
		std::cout << std::endl;

		std::cout << mat[4] << " ";
		std::cout << mat[5] << " ";
		std::cout << mat[6] << " ";
		std::cout << mat[7];
		std::cout << std::endl;

		std::cout << mat[8] << " ";
		std::cout << mat[9] << " ";
		std::cout << mat[10] << " ";
		std::cout << mat[11];
		std::cout << std::endl;

		std::cout << mat[12] << " ";
		std::cout << mat[13] << " ";
		std::cout << mat[14] << " ";
		std::cout << mat[15];
		std::cout << std::endl;
	}
};