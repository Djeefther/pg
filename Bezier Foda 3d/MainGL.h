#pragma once

#include <cstdio>
#include <cstdlib>
#include <ctime>

#include <list>

#include <gl/glut.h>

#include "Matriz.h"
#include "Vetor.h"

//Classe Singleton
class MainGL {
private:
	struct Point {
		//Coords
		GLdouble x;
		GLdouble y;
		GLdouble z;
		Point operator+(const Point& other) {
			return { x + other.x, y + other.y, other.z };
		}
		Point operator*(const GLdouble& other) {
			return {x*other, y*other, z*other};
		}
	};

	static MainGL* _instance;

	MainGL(void);
	~MainGL(void);

	void Draw(void);
	
	std::list<Point> points;

	void WindowResize(GLsizei, GLsizei);

	void MouseHandler(GLint, GLint, GLint, GLint);
	void Motion(GLint, GLint);

	//Toler�ncia pra clique
	GLdouble tolerance;

	bool moving = false;
	std::list<Point>::iterator movingPoint;

	//Polin�mio de bernstein recursivo
	Point B(std::list<Point>::iterator, std::list<Point>::iterator, GLdouble);

	//Polin�mio de bernstein de grau n
	double* B_poly(int, double);

	GLfloat angle, fAspect;

	void VisParameters(void);

	double rot = 0;
	GLdouble wx = 0, wy = 0, wz = 0;
	bool rotating = false;
	Matriz M;
	Matriz K;

	GLint _x, _y;

public:
	void RunLoop(void);

	static MainGL& Instance(void);
};

