#include <stdlib.h>
#include <math.h>
#include <iostream>
#include <list>
#include <vector>
#include <string>
#include <sstream>

#include <GL/glut.h>

#include "utils.h"
#include "Bezier.h"

struct Point;

//Parametros:
#define DELTA_T_BEZIER 0.001
#define WIDTH 800
#define HEIGHT 600
#define RADIUS_SELECT_POINT 0.03

#define ITERATIVE_BEZIER_VELOCITY_INIT 3
#define ITERATIVE_BEZIER_VELOCITY_MIN 1
#define ITERATIVE_BEZIER_VELOCITY_MAX 500

//Parametros de cores e tamanho dos pontos e linhas
const color_t background_help_color = color_uchar(0x1B, 0x58, 0x7B);
const color_t text_help_color = color_uchar(0xEC, 0xD0, 0x78);
const color_t bezier_line_color = { 1.0f, 0.0f, 0.0f };
const float bezier_line_width = 3.0f;
const color_t control_points_color = { 0.0f, 0.3f, 0.3f };
const float control_points_size = 8.0f;
const color_t lines_between_control_points_color = { 0.0f, 0.45f, 0.45f };
const float lines_between_control_points_width = 2.0f;
std::vector<color_t> intermediate_lines_colors;
const float intermediate_lines_width = 1.0f;
std::vector<color_t>& intermediate_points_colors = intermediate_lines_colors;
const float intermediate_points_width = 3.0f;

//Lista das cores usadas no modo iterativo:
void init_intermediate_lines_colors() {
	intermediate_lines_colors.push_back(color_uchar(0, 169, 235));//Bright blue 
	intermediate_lines_colors.push_back(color_uchar(255, 233, 0));//Serdar Yellow
	intermediate_lines_colors.push_back(color_uchar(149, 119, 0));//Orange Crush
	intermediate_lines_colors.push_back(color_uchar(2, 134, 152));//Silent deadly blue
	intermediate_lines_colors.push_back(color_uchar(0, 190, 33));//Zinger Green
	intermediate_lines_colors.push_back(color_uchar(17, 34, 51));////Coldfusion
	intermediate_lines_colors.push_back(color_uchar(80, 0, 255));//Blue Violet
	intermediate_lines_colors.push_back(color_uchar(114, 102, 89));//Ia Mujer Diosa
	intermediate_lines_colors.push_back(color_uchar(251, 176, 52));//Orange
}

//Variaveis globais de controle:
Bezier<Point> bezier;
int point_selected = -1;
std::vector<Point> actual_bezier_points;
bool show_help = true;

//iterative bezier:
bool iterative_bezier = true;
double iterative_bezier_t = 0.0;
bool iterative_bezier_paused = false;
unsigned iterative_bezier_velocity = ITERATIVE_BEZIER_VELOCITY_INIT;

//undo
std::vector<Point> undo_points;
//screen resolution
int screen_width = WIDTH;
int screen_height = HEIGHT;

void reset_iterative_bezier() {
	iterative_bezier_t = 0.0;
	iterative_bezier_paused = false;
	actual_bezier_points.clear();
}

class Vetor {
private:
	GLdouble vec[4];

public:
	Vetor() {
		vec[0] = 0;
		vec[1] = 0;
		vec[2] = 0;
		vec[3] = 0;
	}
	Vetor(GLdouble v1, GLdouble v2, GLdouble v3, GLdouble v4) {
		vec[0] = v1;
		vec[1] = v2;
		vec[2] = v3;
		vec[3] = v4;
	}

	double& operator()(GLint i) {
		return vec[i];
	}
	double operator()(GLint i) const {
		return vec[i];
	}

	Vetor operator+(const Vetor& other) {
		return Vetor(vec[0] + other.vec[0],
			vec[1] + other.vec[1],
			vec[2] + other.vec[2],
			vec[3] + other.vec[3]);
	}

	Vetor operator*(const GLdouble& other) {
		return Vetor(vec[0] + other,
			vec[1] + other,
			vec[2] + other,
			vec[3] + other);
	}
};

class Matriz {
private:
	GLdouble mat[16];

public:
	Matriz(double mat16[16]) {
		mat[0] = mat16[0];
		mat[1] = mat16[1];
		mat[2] = mat16[2];
		mat[3] = mat16[3];

		mat[4] = mat16[4];
		mat[5] = mat16[5];
		mat[6] = mat16[6];
		mat[7] = mat16[7];

		mat[8] = mat16[8];
		mat[9] = mat16[9];
		mat[10] = mat16[10];
		mat[11] = mat16[11];

		mat[12] = mat16[12];
		mat[13] = mat16[13];
		mat[14] = mat16[14];
		mat[15] = mat16[15];
	}
	Matriz() {
		mat[0] = 1;
		mat[1] = 0;
		mat[2] = 0;
		mat[3] = 0;

		mat[4] = 0;
		mat[5] = 1;
		mat[6] = 0;
		mat[7] = 0;

		mat[8] = 0;
		mat[9] = 0;
		mat[10] = 1;
		mat[11] = 0;

		mat[12] = 0;
		mat[13] = 0;
		mat[14] = 0;
		mat[15] = 1;
	}

	double& operator()(GLint j, GLint i) {
		return mat[i * 4 + j];
	}
	double operator()(GLint j, GLint i) const {
		return mat[i * 4 + j];
	}

	Matriz operator*(const Matriz& B) {
		Matriz m;

		Matriz& A = *this;

		m(0, 0) = A(0, 0)*B(0, 0) + A(1, 0)*B(0, 1) + A(2, 0)*B(0, 2) + A(3, 0)*B(0, 3);
		m(1, 0) = A(0, 0)*B(1, 0) + A(1, 0)*B(1, 1) + A(2, 0)*B(1, 2) + A(3, 0)*B(1, 3);
		m(2, 0) = A(0, 0)*B(2, 0) + A(1, 0)*B(2, 1) + A(2, 0)*B(2, 2) + A(3, 0)*B(2, 3);
		m(3, 0) = A(0, 0)*B(3, 0) + A(1, 0)*B(3, 1) + A(2, 0)*B(3, 2) + A(3, 0)*B(3, 3);

		m(0, 1) = A(0, 1)*B(0, 0) + A(1, 1)*B(0, 1) + A(2, 1)*B(0, 2) + A(3, 1)*B(0, 3);
		m(1, 1) = A(0, 1)*B(1, 0) + A(1, 1)*B(1, 1) + A(2, 1)*B(1, 2) + A(3, 1)*B(1, 3);
		m(2, 1) = A(0, 1)*B(2, 0) + A(1, 1)*B(2, 1) + A(2, 1)*B(2, 2) + A(3, 1)*B(2, 3);
		m(3, 1) = A(0, 1)*B(3, 0) + A(1, 1)*B(3, 1) + A(2, 1)*B(3, 2) + A(3, 1)*B(3, 3);

		m(0, 2) = A(0, 2)*B(0, 0) + A(1, 2)*B(0, 1) + A(2, 2)*B(0, 2) + A(3, 2)*B(0, 3);
		m(1, 2) = A(0, 2)*B(1, 0) + A(1, 2)*B(1, 1) + A(2, 2)*B(1, 2) + A(3, 2)*B(1, 3);
		m(2, 2) = A(0, 2)*B(2, 0) + A(1, 2)*B(2, 1) + A(2, 2)*B(2, 2) + A(3, 2)*B(2, 3);
		m(3, 2) = A(0, 2)*B(3, 0) + A(1, 2)*B(3, 1) + A(2, 2)*B(3, 2) + A(3, 2)*B(3, 3);

		m(0, 3) = A(0, 3)*B(0, 0) + A(1, 3)*B(0, 1) + A(2, 3)*B(0, 2) + A(3, 3)*B(0, 3);
		m(1, 3) = A(0, 3)*B(1, 0) + A(1, 3)*B(1, 1) + A(2, 3)*B(1, 2) + A(3, 3)*B(1, 3);
		m(2, 3) = A(0, 3)*B(2, 0) + A(1, 3)*B(2, 1) + A(2, 3)*B(2, 2) + A(3, 3)*B(2, 3);
		m(3, 3) = A(0, 3)*B(3, 0) + A(1, 3)*B(3, 1) + A(2, 3)*B(3, 2) + A(3, 3)*B(3, 3);

		return m;
	}

	void translate(GLdouble x, GLdouble y, GLdouble z) {
		Matriz& A = *this;
		Matriz B;

		B(0, 3) = x;
		B(1, 3) = y;
		B(2, 3) = z;

		A = A*B;
	}

	void rotateZ(GLdouble theta) {
		Matriz& A = *this;

		Matriz B;

		B(0, 0) = cos(theta);
		B(0, 1) = -sin(theta);
		B(1, 0) = sin(theta);
		B(1, 1) = cos(theta);

		A = A*B;
	}

	void rotateX(GLdouble theta) {
		Matriz& A = *this;

		Matriz B;

		B(1, 1) = cos(theta);
		B(1, 2) = -sin(theta);
		B(2, 1) = sin(theta);
		B(2, 2) = cos(theta);

		A = A*B;
	}

	void rotateY(GLdouble theta) {
		Matriz& A = *this;

		Matriz B;

		B(0, 0) = cos(theta);
		B(0, 2) = sin(theta);
		B(2, 0) = -sin(theta);
		B(2, 2) = cos(theta);

		A = A*B;
	}

	void rotateW(GLdouble theta, GLdouble wx, GLdouble wy, GLdouble wz) {
		//Normaliza, s� por garantia
		GLdouble sq = sqrt(wx*wx + wy*wy + wz*wz);
		wx = wx / sq;
		wy = wy / sq;
		wz = wz / sq;

		Matriz& A = *this;

		Matriz B;

		B(0, 0) = cos(theta) + wx*wx*(1 - cos(theta));
		B(0, 1) = wx*wy*(1 - cos(theta)) - wz*sin(theta);
		B(0, 2) = wy*sin(theta) + wx*wz*(1 - cos(theta));
		B(1, 0) = wz*sin(theta) + wx*wy*(1 - cos(theta));
		B(1, 1) = cos(theta) + wy*wy*(1 - cos(theta));
		B(1, 2) = -wx*sin(theta) + wy*wz*(1 - cos(theta));
		B(2, 0) = -wy*sin(theta) + wx*wz*(1 - cos(theta));
		B(2, 1) = wx*sin(theta) + wy*wz*(1 - cos(theta));
		B(2, 2) = cos(theta) + wz*wz*(1 - cos(theta));

		A = A*B;
	}

	void scale(GLdouble sx, GLdouble sy, GLdouble sz) {
		Matriz& A = *this;

		Matriz B;

		B(0, 0) = sx;
		B(1, 1) = sy;
		B(2, 2) = sz;

		A = A*B;
	}

	const GLdouble* get() {
		return mat;
	}

	void print() {
		std::cout << mat[0] << " ";
		std::cout << mat[1] << " ";
		std::cout << mat[2] << " ";
		std::cout << mat[3];
		std::cout << std::endl;

		std::cout << mat[4] << " ";
		std::cout << mat[5] << " ";
		std::cout << mat[6] << " ";
		std::cout << mat[7];
		std::cout << std::endl;

		std::cout << mat[8] << " ";
		std::cout << mat[9] << " ";
		std::cout << mat[10] << " ";
		std::cout << mat[11];
		std::cout << std::endl;

		std::cout << mat[12] << " ";
		std::cout << mat[13] << " ";
		std::cout << mat[14] << " ";
		std::cout << mat[15];
		std::cout << std::endl;
	}
};

void changeSize(int w, int h) {

	if (h == 0)
		h = 1;
	float ratio = w * 1.0 / h;


	glMatrixMode(GL_PROJECTION);


	glLoadIdentity();


	glViewport(0, 0, w, h);


	gluPerspective(45.0f, ratio, 0.1f, 100.0f);


	glMatrixMode(GL_MODELVIEW);
}

double rot = 0;
GLdouble wx = 0, wy = 0, wz = 0;
bool rotating = false;
Matriz M;
Matriz K;

struct Point {
	//Coords
	GLdouble x;
	GLdouble y;
	GLdouble z;
	Point operator+(const Point& other) const {
		return{ x + other.x, y + other.y, z + other.z };
	}
	Point operator-(const Point& other) const {
		return{ x - other.x, y - other.y, z - other.z };
	}
	Point operator*(const GLdouble& other) const {
		return{ x*other, y*other, z*other };
	}
};

double abs(const Point& p) {
	return sqrt(p.x*p.x + p.y*p.y + p.z*p.z);
}

//std::list<Point> ptrs;

double* B_poly(int n, double t) {
	double* bern;
	int i;
	int j;

	bern = new double[n + 1];

	if (n == 0) {
		bern[0] = 1.;
	}
	else if (0 < n) {
		bern[0] = 1.0 - t;
		bern[1] = t;

		for (i = 2; i <= n; ++i) {
			bern[i] = t * bern[i - 1];

			for (j = i - 1; 1 <= j; --j) {
				bern[j] = t * bern[j - 1] + (1. - t) * bern[j];
			}

			bern[0] = (1. - t) * bern[0];
		}
	}

	return bern;
}

//void renderScene(void) {
//	//std::cout << matrix[11] << std::endl;
//	//matrix[11] -= 0.001;
//
//	// Clear Color and Depth Buffers
//	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
//
//	glMatrixMode(GL_MODELVIEW);
//
//	M.translate(0, 0, 6);
//
//	//Matriz M;
//	//M.rotateZ(i += 0.01);
//
//	K = M;
//
//	if (rotating) {
//		K.rotateW(rot / 100, wx, wy, wz);
//
//		K.translate(0, 0, -6);
//	}
//
//	M.translate(0, 0, -6);
//
//	if (rotating)
//		glLoadMatrixd(K.get());
//	else
//		glLoadMatrixd(M.get());
//
//
//	if (ptrs.size() >= 2) {
//
//		glBegin(GL_LINE_STRIP);
//
//		glColor3f(0.3f, 0.3f, 0.3f);
//
//		for (GLdouble t = 0; t <= 1 + 0.01; t += 0.01) {
//			//Point P = B(points.begin(), --points.end(), t);
//
//			double* polys = B_poly(ptrs.size() - 1, t);
//
//			auto it = ptrs.begin();
//			int i;
//			Point P = { 0, 0 };
//			for (i = 0; i < ptrs.size(); ++i) {
//				P = P + (*it) * polys[i];
//
//				it++;
//			}
//
//			glVertex3d(P.x, P.y, P.z);
//			//std::cout << t << std::endl;
//
//			free(polys);
//
//		}
//
//		glEnd();
//	}
//
//	glColor3f(1.0f, 0.0f, 0.0f);
//
//	glBegin(GL_POINTS);
//
//	for (auto it = ptrs.begin(); it != ptrs.end(); ++it) {
//		glVertex3d(it->x, it->y, it->z);
//	}
//
//	glEnd();
//
//	glutSwapBuffers();
//}

void keybord(unsigned char c, int x, int y) {
	switch (tolower(c))
	{
	case 'c': {
		for (auto it = bezier.control_points().rbegin(); it != bezier.control_points().rend(); it++) {
			undo_points.push_back(*it);
		}
		bezier.control_points().clear();

		point_selected = -1;
		reset_iterative_bezier();
		glutPostRedisplay();
		break;
	}
	case 8: {//backspace
		if (bezier.control_points().size() > 0) {
			undo_points.push_back(bezier.control_points().back());
			bezier.control_points().pop_back();

			point_selected = -1;
			reset_iterative_bezier();
			glutPostRedisplay();
		}
		break;
	}
	case 26: {//ctrl+z
		if (undo_points.size() > 0) {
			bezier.control_points().push_back(undo_points.back());
			undo_points.pop_back();

			point_selected = -1;
			reset_iterative_bezier();
			glutPostRedisplay();
		}
		break;
	}
	case 'h': {
		show_help = !show_help;
		glutPostRedisplay();
		break;
	}
	case '\r': { //Enter
		show_help = false;
		glutPostRedisplay();
		break;

	}
	case ' ': {
		iterative_bezier_paused = !iterative_bezier_paused;
		break;
	}
	case 'i': {
		iterative_bezier = !iterative_bezier;
		reset_iterative_bezier();
		glutPostRedisplay();
		break;
	}
	case '=':
	case '+': {
		if (iterative_bezier) {
			iterative_bezier_velocity = std::min(iterative_bezier_velocity + 1, (unsigned)ITERATIVE_BEZIER_VELOCITY_MAX);
		}
		break;
	}
	case '-':
	case '_': {
		if (iterative_bezier) {
			iterative_bezier_velocity = std::max(iterative_bezier_velocity - 1, (unsigned)ITERATIVE_BEZIER_VELOCITY_MIN);
		}
		break;
	}
	default:
		break;
	}
}


void draw_text() {
	if (show_help) {
		auto font = GLUT_BITMAP_8_BY_13;
		int char_height = 13;
		int char_width = 8;
		int margin = 4;

		int lines = 13;
		int coluns = 40;

		int text_height = lines * char_height;
		int text_width = coluns * char_width;

		int x = screen_width / 2 - text_width / 2;
		int y = screen_height / 2 - text_height / 2;


		auto pos_inc_y = [&](){
			auto aux = y;
			y += char_height;
			return aux;
		};

		//Desenha um quadrado ao fundo
		glColor_t(background_help_color);
		glBegin(GL_QUADS); {
			int x_begin = x - margin;
			int x_end = x + text_width + margin;
			int y_begin = y - char_height - margin;
			int y_end = y - char_height + text_height + margin;

			glVertex2i(x_begin, y_begin);
			glVertex2i(x_end, y_begin);
			glVertex2i(x_end, y_end);
			glVertex2i(x_begin, y_end);
		}

		glEnd();

		//Desenha o texto
		glPrintText(x + text_width / 2 - (char_width * 4) / 2, pos_inc_y(), text_help_color, font, "HELP");
		pos_inc_y();
		glPrintText(x, pos_inc_y(), text_help_color, font, "H:          Help");
		glPrintText(x, pos_inc_y(), text_help_color, font, "Click:      Add or move a point");
		glPrintText(x, pos_inc_y(), text_help_color, font, "Backspace:  Remove last added point");
		glPrintText(x, pos_inc_y(), text_help_color, font, "C:          Remove all points");
		glPrintText(x, pos_inc_y(), text_help_color, font, "CTRL+Z:     Undo a remove");
		glPrintText(x, pos_inc_y(), text_help_color, font, "I:          Toggle iterative mode");
		glPrintText(x, pos_inc_y(), text_help_color, font, "+:          Speed up the iterative mode");
		glPrintText(x, pos_inc_y(), text_help_color, font, "-:          Slow down the iterative mode");
		glPrintText(x, pos_inc_y(), text_help_color, font, "Spacebar:   Pause iterative mode");
		glPrintText(x, pos_inc_y(), text_help_color, font, "R:          Randomly add a point");
		glPrintText(x, pos_inc_y(), text_help_color, font, "Enter:      Close this window");
	}

	//Printando dados gerais
	{
		std::stringstream ss;
		int x = screen_width - 110;
		int y = 20;
		int char_height = 13;

		auto pos_inc_y = [&](){
			auto aux = y;
			y += char_height;
			return aux;
		};


		ss << "Points: " << bezier.control_points().size();
		glPrintText(x, pos_inc_y(), color(0, 0, 0), GLUT_BITMAP_8_BY_13, ss.str().c_str());
		ss.str(std::string());

		if (iterative_bezier) {
			ss << "Velocity: " << iterative_bezier_velocity;
			glPrintText(x, pos_inc_y(), color(0, 0, 0), GLUT_BITMAP_8_BY_13, ss.str().c_str());
			ss.str(std::string());
		}

		if (iterative_bezier_paused) {
			glPrintText(x, pos_inc_y(), color(0, 0, 0), GLUT_BITMAP_8_BY_13, "Paused");
		}
	}

#if _DEBUG
	{
		int x = screen_width / 2 - (20 * 18) / 2;
		int y = 20;
		glPrintText(x, y, color(255, 0, 0), GLUT_BITMAP_HELVETICA_18, "WARNING: COMPILED IN DEBUG MODE!!!!");
		glPrintText(x, y + 18, color(255, 0, 0), GLUT_BITMAP_HELVETICA_18, "PLEASE REBUILD IN RELEASE MODE!!!!");
	}
#endif

}

void draw() {
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glMatrixMode(GL_MODELVIEW);
	M.translate(0, 0, 6);
	//Matriz M;
	//M.rotateZ(i += 0.01);
	K = M;
	if (rotating) {
		K.rotateW(rot / 100, wx, wy, wz);
		K.translate(0, 0, -6);
	}
	M.translate(0, 0, -6);

	if (rotating)
		glLoadMatrixd(K.get());
	else
		glLoadMatrixd(M.get());


	auto draw_strip_line = [](const std::vector<Point>& points) {
		glBegin(GL_LINE_STRIP);
		for (const auto& i : points) {
			glVertex3d(i.x, i.y , i.z);
		}
		glEnd();
	};

	auto draw_points = [](const std::vector<Point>& points) {
		glBegin(GL_POINTS);
		for (const auto& i : points) {
			glVertex3d(i.x, i.y, i.z);
		}
		glEnd();
	};

	if (bezier.valid()) {
		//Computa pontos de bezier (todos se n�o estiver no modo iterativo)
		if (iterative_bezier) {
			if (!iterative_bezier_paused) {
				for (unsigned i = 0; i < iterative_bezier_velocity; i++)  {
					actual_bezier_points.emplace_back(bezier.point(std::min(iterative_bezier_t, 1.0)));
					iterative_bezier_t += DELTA_T_BEZIER;
				}
			}
		}
		else {
			for (double t = 0; t < 1.0 + DELTA_T_BEZIER; t += DELTA_T_BEZIER) {
				actual_bezier_points.emplace_back(bezier.point(std::min(t, 1.0)));
			}
		}

		if (iterative_bezier) {
			//Desenha as linhas auxiliares do bezier (apenas modo iterativo)
			size_t color_level = 0;
			size_t color_level_limit = intermediate_lines_colors.size() - 1;
			glLineWidth(intermediate_lines_width);
			for (const auto& l : bezier.auxiliar_points()) {
				glColor_t(intermediate_lines_colors[color_level]);
				draw_strip_line(l);
				color_level = std::min(color_level + 1, color_level_limit);
			}


			//Desenha os pontos de encontro das linhas auxiliares do bezier
			color_level = 0;
			color_level_limit = intermediate_lines_colors.size() - 1;
			glPointSize(intermediate_points_width);
			for (const auto& l : bezier.auxiliar_points()) {
				glColor_t(intermediate_points_colors[color_level]);
				draw_points(l);
				color_level = std::min(color_level + 1, color_level_limit);
			}
		}
	}

	if (bezier.control_points().size() > 0) {
		//Retas entre pontos de controle
		glColor_t(lines_between_control_points_color);
		glLineWidth(lines_between_control_points_width);
		draw_strip_line(bezier.control_points());

		//Pontos de controle
		glColor_t(control_points_color);
		glPointSize(control_points_size);
		draw_points(bezier.control_points());
	}

	if (bezier.valid()) {
		//Desenha a bezier (de forma completa ou n�o)
		glColor_t(bezier_line_color);
		glLineWidth(bezier_line_width);
		draw_strip_line(actual_bezier_points);

		//Checagens para o pr�ximo draw
		if (iterative_bezier) {
			if (iterative_bezier_t >= 1.0) {
				reset_iterative_bezier();
			}
		}
		else {
			actual_bezier_points.clear();
		}
	}

	//draw_text();
	glutSwapBuffers();
	//if (iterative_bezier)
	//	glutPostRedisplay();
}

int clicked_point(const std::vector<Point>& points, const Point& click) {
	int i = 0;
	for (const auto& p : points) {
		double distance = abs(p - click);
		//PRINTLN_VALUE(distance);
		if (distance <= RADIUS_SELECT_POINT) {
			return i;
		}
		i++;
	}
	return -1;
}

GLint _x, _y;


void movement_mouse(int x, int y) {
	auto magic_rotate = [&]()->Point{
		GLfloat winX = 0.0, winY = 0.0, winZ = 0.0;
		GLdouble px = 0., py = 0., pz = 0.;

		GLint OGLMviewport[4];
		GLdouble OGLMprojection[16];
		GLdouble OGLMmodelview[16];

		glGetDoublev(GL_PROJECTION_MATRIX, OGLMprojection);
		glGetDoublev(GL_MODELVIEW_MATRIX, OGLMmodelview);
		glGetIntegerv(GL_VIEWPORT, OGLMviewport);

		winX = (float)x;
		winY = (float)OGLMviewport[3] - (float)y;   // Inverte winY

		glReadPixels(x, int(winY), 1, 1, GL_DEPTH_COMPONENT, GL_FLOAT, &winZ);
		std::cout << winZ << std::endl;
		gluUnProject(winX, winY, winZ - 0.0157/* (numero magico) */, OGLMmodelview, OGLMprojection, OGLMviewport, &px, &py, &pz);
		return{ px, py, pz };
	};

	if (point_selected >= 0) {
		bezier.control_points()[point_selected] = magic_rotate();

		undo_points.clear();
		reset_iterative_bezier();
		//glutPostRedisplay();
	}
}

void mouseHandler(GLint button, GLint state, GLint x, GLint y) {
	if (button == GLUT_MIDDLE_BUTTON) {
		point_selected = -1;
		if (state == GLUT_DOWN) {
			rotating = true;
			_x = x;
			_y = y;
		}
		else if (state == GLUT_UP) {
			rotating = false;
			rot = 0;
			M = K;
		}
	}

	auto magic_rotate = [&]()->Point{
		GLfloat winX = 0.0, winY = 0.0, winZ = 0.0;
		GLdouble px = 0., py = 0., pz = 0.;

		GLint OGLMviewport[4];
		GLdouble OGLMprojection[16];
		GLdouble OGLMmodelview[16];

		glGetDoublev(GL_PROJECTION_MATRIX, OGLMprojection);
		glGetDoublev(GL_MODELVIEW_MATRIX, OGLMmodelview);
		glGetIntegerv(GL_VIEWPORT, OGLMviewport);

		winX = (float)x;
		winY = (float)OGLMviewport[3] - (float)y;   // Inverte winY

		glReadPixels(x, int(winY), 1, 1, GL_DEPTH_COMPONENT, GL_FLOAT, &winZ);
		std::cout << winZ << std::endl;
		gluUnProject(winX, winY, winZ - 0.0157/* (numero magico) */, OGLMmodelview, OGLMprojection, OGLMviewport, &px, &py, &pz);
		return {px,py,pz};
	};

	if (button == GLUT_LEFT_BUTTON) {
		if (state == GLUT_DOWN) {
			Point p = magic_rotate();
			//ptrs.push_back({ px, py, pz });
			auto actual_point_selected = clicked_point(bezier.control_points(), p);

			if (actual_point_selected >= 0) {
				point_selected = actual_point_selected;
			}
			else {
				bezier.control_points().emplace_back(p);

				point_selected = actual_point_selected;//Remover a sele��o a algum ponto, caso exista
				undo_points.clear();
				reset_iterative_bezier();

				//glutPostRedisplay();
			}
		}
		else if (state == GLUT_UP) {
			point_selected = -1;
		}
	}
}

void motionHandler(GLint x, GLint y) {
	if (rotating) {
		GLint dx = x - _x;
		GLint dy = y - _y;

		rot = sqrt(dx*dx + dy*dy);

		wy = dx;
		wx = dy;
	}

	movement_mouse(x, y);
}


int main(int argc, char **argv) {
	init_intermediate_lines_colors();
	// init GLUT and create window
	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_DEPTH | GLUT_DOUBLE | GLUT_RGBA);
	glutInitWindowPosition(100, 100);
	glutInitWindowSize(800, 600);
	glutCreateWindow("Bezier 3D");


	glEnable(GL_POINT_SMOOTH);
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	glPointSize(6.0);
	glLineWidth(3.0);


	M.translate(0, 0, -6);

	glClearColor(1.0f, 1.0f, 1.0f, 1.0f);

	// register callbacks
	glutDisplayFunc(draw);
	glutReshapeFunc(changeSize);
	glutIdleFunc(draw);
	glutMouseFunc(mouseHandler);
	glutMotionFunc(motionHandler);
	glutKeyboardFunc(keybord);

	// enter GLUT event processing cycle
	glutMainLoop();

	return 1;
}